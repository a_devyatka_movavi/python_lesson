#!/usr/bin/env python3
# -*- coding=utf-8

class Student:
    """
    Класс описывает студента
    
    name - имя студента
    age  - возраст
    term - текущий семестр
    subj - посещаемые предметы/дисциплины
    """

    def __init__(self, name, age, term=None):
        self.name = name
        self.age  = age
        self.term = term
        self.subj = []
