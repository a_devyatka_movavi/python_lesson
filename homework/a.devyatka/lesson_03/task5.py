#!/usr/bin/env python3
# -*- coding=utf-8

import task4

def is_learn(student, subj_name):
    """
    Функция проверяет, изучает ли студент предмет
    """
    for s in student.subj:
        if s.name == subj_name and s.term == student.term:
            return True
    return False

def is_exam_mark(student, subj_name, mark):
    """
    Функция прверяет, сдал ли студет экзамен на заданую оценку
    """
    for s in student.subj:
        if s.name == subj_name and s.mark == mark:
            return True
        return False

def calc_count_mark(student):
    """
    Считает кол-во сданных экзаменов
    """
    cnt = 0
    for s in student.subj:
        if s.mark is not None:
            cnt += 1
    return cnt

def calc_average_mark(student):
    """
    Считает средний балл студента
    """
    sum = 0.0
    cnt = 0
    for s in student.subj:
        if s.mark is not None:
            sum += s.mark
            cnt += 1
    return sum / cnt if cnt else 0.0

def main():
    # Часьт 1
    print('Студенты изучающие физику:')
    for s in task4.all_students: is_learn(s, 'Физика') and print(s.name)
    print()
    
    # Часть 2
    print('Студенты изучающие химию и сдавшие физику на 4+:')
    for s in task4.all_students:
        if is_learn(s, 'Химия') and \
                (is_exam_mark(s, 'Физика', 4) or is_exam_mark(s, 'Физика', 5)):
            print(s.name)
    print()

    # Часть 3
    print('Студенты моложе 20 со средним балом не ниже 4.5:')
    for s in task4.all_students:
        if s.age < 20:
            m = calc_average_mark(s)
            if m >= 4.5:
                print(s.name, s.age, m)
    print()

    # Часть 4
    print('Топ5 студенов сдавшх более 2-х предметов:')
    rank = []
    for s in task4.all_students:
        cnt_mark = calc_count_mark(s)
        if cnt_mark < 3: continue
        m = calc_average_mark(s)
        data = { 'student': s, 'cnt_mark': cnt_mark, 'avr_mark': m, }
        rank.append(data)
    rank.sort(key=lambda k: k['avr_mark'], reverse=True)
    for d in rank[:5]:
        s = d['student']
        print(s.name, s.term, d['avr_mark'], d['cnt_mark'])
    print()

if __name__ == '__main__':
    main()
