#!/usr/bin/env python3
# -*- coding=utf-8

from task2 import Student

class Subject:
    """
    Класс описывает изучаемую дисциплину (предмет)

    name - название предмета
    term - семест на котором предмет преподавался
    mark - балл полученный на экзамене
    """

    def __init__(self, name, term=None, mark=None):
        self.name = name
        self.term = term
        self.mark = mark
