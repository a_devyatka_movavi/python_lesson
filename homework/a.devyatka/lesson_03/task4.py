#!/usr/bin/env python3
# -*- coding=utf-8

from task3 import Student, Subject

def apply_term1(student):
    """
    Функция "зачисляет" студента на первый курс некоторой специальности
    """
    student.term = 1
    student.subj.extend([
        Subject('Физика', term=1),
        Subject('Химия',  term=1),
        Subject('Ин. яз', term=1),
        ])

def apply_term2(student):
    """
    Функция "зачисляет" студента на второй курс некоторой специальности
    """
    student.term = 2
    student.subj.extend([
        Subject('Физика',     term=2),
        Subject('Математика', term=2),
        Subject('Экономика',  term=2),
        ])

def apply_term3(student):
    """
    Функция "зачисляет" студента на третий курс некоторой специальности
    """
    student.term = 3
    student.subj.extend([
        Subject('Геология', term=3),
        Subject('Экология', term=3),
        Subject('Зоология', term=3),
        ])

def set_exam_mark(student, subj_name, subj_term, mark):
    """
    Функция устанавлиет отметку о сдаче экзамена
    """
    for subj in student.subj:
        if subj.name == subj_name and subj.term == subj_term:
            subj.mark = mark

st00 = Student('Артём',     18)
st01 = Student('Андрей',    17)
st02 = Student('Владимир',  17)
st03 = Student('Иван',      18)
st04 = Student('Кирилл',    17)
st05 = Student('Марина',    17)
st06 = Student('Роман',     18)
gr0 = [st00, st01, st02, st03, st04, st05, st06]
for s in gr0: apply_term1(s)
set_exam_mark(st00, 'Физика', 1, 3)
set_exam_mark(st01, 'Физика', 1, 4)
set_exam_mark(st02, 'Физика', 1, 3)
set_exam_mark(st03, 'Физика', 1, 5)

st10 = Student('Ольга',     19)
st11 = Student('Олеся',     18)
st12 = Student('Максим',    19)
st13 = Student('Михаил',    18)
st14 = Student('Дмитрий',   18)
st15 = Student('Татьяна',   19)
gr1 = [st10, st11, st12, st13, st14, st15]
for s in gr1: apply_term1(s); apply_term2(s)
set_exam_mark(st10, 'Физика', 1, 3)
set_exam_mark(st10, 'Физика', 2, 4)
set_exam_mark(st11, 'Физика', 1, 4)
set_exam_mark(st11, 'Физика', 2, 4)
set_exam_mark(st11, 'Ин. яз', 1, 5)
set_exam_mark(st12, 'Физика', 1, 3)
set_exam_mark(st13, 'Физика', 1, 5)

st20 = Student('Александр', 19)
st21 = Student('Пётр',      20)
st22 = Student('Денис',     19)
st23 = Student('Алексей',   18)
st24 = Student('Ирина',     19)
st25 = Student('Анна',      19)
gr2 = [st20, st21, st22, st23, st24, st25]
for s in gr2: apply_term1(s); apply_term2(s); apply_term3(s)

all_groups = [gr0, gr1, gr2]
all_students = []
for g in all_groups: all_students.extend(g)
