# README
## Уроки python для QA

[Занятия](lessons)

## Ссылки для самостоятельного изучения Python
[Уроки devpractice.ru](http://devpractice.ru/python-lessons/)

[Самоучитель pythonworld.ru](https://pythonworld.ru/samouchitel-python)

[Python. Введение в программирование](http://younglinux.info/python.php)

[Уроки Python](http://dafter.ru/duf/programming/46.html)
