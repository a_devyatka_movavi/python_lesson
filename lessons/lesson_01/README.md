﻿# Краткое описание занятия

## Python2 vs Python3

## Вывод (print)
```
>>> print(1)
1
>>> print(2)
2
>>> print('Hi!')
Hi!
>>> print('Hi!', 1, 2, 3)
('Hi!', 1, 2, 3)
```

## Арифметика
```
>>> 5+3 # сложение
8
>>> 5-2 # вычитание
3
>>> 5*3 # умножение
15
>>> 5.0/3 # деление
1.6666666666666667
>>> 5//3 # целочисленное деление
1
>>> 5%3 # остаток от деления
2
```
## Переменные, типы
Из вики: Переменная в императивном программировании — поименованная, либо адресуемая иным способом область памяти, адрес которой можно использовать для осуществления доступа к данным. Данные, находящиеся в переменной (то есть по данному адресу памяти), называются значением этой переменной.

Если проще, то переменная - это именованная область данных (что-то имеющее имя и хранящее данные).

В питоне для присвоения переменной данных используется символ `=`.

Синтаксис: `<имя> = <данные>`.
```
>>> a = 5 # В переменной с именем a будет присвоено значение 5
>>> b = 2.1 # Переменной b присваиваем значение 2.1
```

### Числа
### Списки
```
>>> a = [] # В a будет пустой список
>>> b = [1, 2, 3] # В b будет список из трёх элементов 1, 2 и 3
>>> c = [5, [9, 8], b] # В c будет список из числа 5, списка [9, 8] и списка [1, 2, 3]
>>> print(c)
[5, [9, 8], [1, 2, 3]]
```
### Строки
В python строки можно рассматривать как список букв.
Строки могут заключаться в одинарные кавычки `'`, двойные кавычки `"`
```
>>> a = 'Hello'
>>> b = "world"
>>> c = a + ', ' + b + "!"
>>> print(c)
'Hello, world!'
```
## Преобразование типов
Для того чтобы узнать тип переменной, используется функция `type`
```
>>> a = 5
>>> type(a)
<type 'int'>
>>> b = 1.0
>>> type(b)
<type 'float'>
>>> c = a * b
>>> type(c)
<type 'float'>
>>> d = 'Hello'
>>> type(d)
<type 'str'>
```
Иногда бывает потребность преобразовать один тип в другой:
```
>>> a = 1
>>> b = float(a)
>>> type(a)
<type 'int'>
>>> type(b)
<type 'float'>
>>> b
1.0
>>> a = '1'
>>> b = '2'
>>> a + b
'12'
>>> int(a) + int(b)
3
```
## Логические операции и условное выполнение (if)
Есть логический тип данных `bool`, он имеет два значения `True` - истина, `False` - ложь.
Также имеются логические операции, результат которых имеет тип `bool`:
```
>>> 1 < 2 # меньше
True
>>> 1 > 2 # больше
False
>>> 1 == 2 # эквивалентно (равно)
False
>>> 1 != 2 # не равно
True
>>> 1 <= 2 # меньше или равно
True
>>> 1 >= 2 # больше или равно
False
```

Над типом bool допустимы следующие операции (not, or, and):
```
>>> not True
False
>>> not False
True
>>> True or True
True
>>> True or False
True
>>> False or False
False
>>> True and True
True
>>> True and False
False
>>> False and False
False

```

Иногда, в зависимости от условия, в программе нужно выполнять различные действия. Для этого используется оператор `if`
```
>>> if 3 < 2:
...     print("Три меньше двух")
... elif 5 >= 10:
...     print("Число пять больше или равно числу 10")
... else:
...     print("Указанные выше утверждения неверны")
... 
Указанные выше утверждения неверны
```
В операторе `if` обязательным является только первый блок, т.к. блоки `elif` и `else` могут отсутствовать.

## Цикл for
Часто приходится выполнять одинаковые действия над списком значений. Для таких задач используется цикл `for`:
```
>>> for a in [1, 2, 3, 4]:
...     print('Processing element, ' + str(a))
... 
Processing element, 1
Processing element, 2
Processing element, 3
Processing element, 4
```
Этот блок кода можно прочитать так: для каждого элемента из списка `[1, 2, 3, 4]` записать его значение в переменную `a` и выполнить действие `print('Processing element, ' + str(a))`

Иногда требуется какое-то действие выполнить фиксированное количество раз, тогда можно использовать функцию/генератор range:
```
>>> for a in range(3):
...     print(a)
... 
0
1
2
>>> for a in range(5, 9):
...     print(a)
... 
5
6
7
8

```

# Задачи для самостоятельного решения

## 1. Простые числа
Написать программу, которая выводит все простые числа от 1 до 199.

## 2. Сумма чисел
2.1. Написать программу, которая выводит сумму всех целых чисел от 0 до 100
2.2. Написать программу, которая выводит сумму всех чётных чисел от 0 до 100
2.3. Написать программу, которая выводит сумму всех нечётных чисел от 0 до 100

## 3. Fizz buzz
Суть игры: игроки (от двух человек) по очереди вслух считают от 1 и до …(пока не надоест). Если число, которое игрок должен озвучить, делится на 3, то он должен сказать fizz. Если число делится на 5 - надо произнести buzz. Если число делится и на 3 и на 5, то надо говорить fizz buzz. Ошибаться нельзя. 
Последовательность должна быть такой: 1, 2, Fizz, 4, Buzz, Fizz, 7, 8, Fizz, Buzz, 11, Fizz, 13, 14, Fizz Buzz, 16, 17, Fizz, 19, Buzz, Fizz, 22, 23, Fizz, Buzz, 26, Fizz, 28, 29, Fizz Buzz, 31, 32, Fizz, 34, Buzz, Fizz, …
Написать программу, которая выводит на экран последовательность игры fizz buzz от 1 до 100.

